/*
 * Copyright (c) 2016 AndreAle
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package it.andreale.theme.engine;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

import it.andreale.theme.ThemeType;
import it.andreale.theme.util.ColorUtils;

/**
 * Created by AndreAle on 03/01/2016.
 */
public class ThemeEngine {

    public static ThemeEngine mInstance;

    public static void initialize(Builder builder) {
        if (builder == null) {
            throw new IllegalStateException("You must provide a valid builder instance");
        }
        if (mInstance == null) {
            mInstance = new ThemeEngine(builder);
        }
    }

    private Context mContext;
    private int mColorPrimary;
    private int mColorPrimaryDark;
    private int mColorAccent;
    private ThemeType mThemeType;
    private int[] mTextPrimaryColors;
    private int[] mTextSecondaryColors;
    private int[] mWindowBackgroundColors;
    private int[] mDisabledHintColors;
    private int[] mDividersColors;
    private ArrayList<ThemeLifeCycle> mLifeCycles;

    private ThemeEngine(Builder builder) {
        mContext = builder.mContext;
        mColorPrimary = Preferences.getColorPrimary(mContext, builder.mColorPrimary);
        mColorPrimaryDark = Preferences.getColorPrimaryDark(mContext, builder.mColorPrimaryDark);
        mColorAccent = Preferences.getColorAccent(mContext, builder.mColorAccent);
        mThemeType = Preferences.getThemeType(mContext, builder.mThemeType);
        mTextPrimaryColors = builder.mTextPrimaryColors != null ? builder.mTextPrimaryColors : ColorUtils.getDefaultTextPrimaryColors();
        mTextSecondaryColors = builder.mTextSecondaryColors != null ? builder.mTextSecondaryColors : ColorUtils.getDefaultTextSecondaryColors();
        mWindowBackgroundColors = builder.mWindowBackgroundColors != null ? builder.mWindowBackgroundColors : ColorUtils.getDefaultWindowBackgroundColors();
        mDisabledHintColors = builder.mDisabledHintColors != null ? builder.mDisabledHintColors : ColorUtils.getDefaultDisabledHintColors();
        mDividersColors = builder.mDividersColors != null ? builder.mDividersColors : ColorUtils.getDefaultDividersColors();
        mLifeCycles = new ArrayList<>();
    }

    public static ThemeEngine getInstance() {
        if (mInstance == null) {
            throw new IllegalStateException("You must initialize ThemeEngine before accessing it");
        }
        return mInstance;
    }

    public void register(ThemeLifeCycle lifeCycle) {
        if (lifeCycle != null) {
            mLifeCycles.add(lifeCycle);
        }
    }

    public void unregister(ThemeLifeCycle lifeCycle) {
        mLifeCycles.remove(lifeCycle);
    }

    public int getColorPrimary() {
        return mColorPrimary;
    }

    public int getColorPrimaryDark() {
        return mColorPrimaryDark;
    }

    public int getColorAccent() {
        return mColorAccent;
    }

    public ThemeType getThemeType() {
        return mThemeType;
    }

    public int getTextPrimaryColor() {
        switch (mThemeType) {
            case THEME_LIGHT:
                return mTextPrimaryColors[0];
            case THEME_DARK:
                return mTextPrimaryColors[1];
            case THEME_DEEP_DARK:
                return mTextPrimaryColors[2];
        }
        return mTextPrimaryColors[0];
    }

    public int getTextSecondaryColor() {
        switch (mThemeType) {
            case THEME_LIGHT:
                return mTextSecondaryColors[0];
            case THEME_DARK:
                return mTextSecondaryColors[1];
            case THEME_DEEP_DARK:
                return mTextSecondaryColors[2];
        }
        return mTextSecondaryColors[0];
    }

    public int getIconColor() {
        switch (mThemeType) {
            case THEME_LIGHT:
                return mTextSecondaryColors[0];
            case THEME_DARK:
                return mTextSecondaryColors[1];
            case THEME_DEEP_DARK:
                return mTextSecondaryColors[2];
        }
        return mTextSecondaryColors[0];
    }

    public int getWindowBackgroundColor() {
        switch (mThemeType) {
            case THEME_LIGHT:
                return mWindowBackgroundColors[0];
            case THEME_DARK:
                return mWindowBackgroundColors[1];
            case THEME_DEEP_DARK:
                return mWindowBackgroundColors[2];
        }
        return mWindowBackgroundColors[0];
    }

    public int getDisabledColor() {
        switch (mThemeType) {
            case THEME_LIGHT:
                return mDisabledHintColors[0];
            case THEME_DARK:
                return mDisabledHintColors[1];
            case THEME_DEEP_DARK:
                return mDisabledHintColors[2];
        }
        return mDisabledHintColors[0];
    }

    public int getHintColor() {
        switch (mThemeType) {
            case THEME_LIGHT:
                return mDisabledHintColors[0];
            case THEME_DARK:
                return mDisabledHintColors[1];
            case THEME_DEEP_DARK:
                return mDisabledHintColors[2];
        }
        return mDisabledHintColors[0];
    }

    public int getDividerColor() {
        switch (mThemeType) {
            case THEME_LIGHT:
                return mDividersColors[0];
            case THEME_DARK:
                return mDividersColors[1];
            case THEME_DEEP_DARK:
                return mDividersColors[2];
        }
        return mDividersColors[0];
    }

    public void applyTheme(View view) {
        if (view instanceof ThemeCallback) {
            // call specific interface method
            ((ThemeCallback) view).applyTheme(mColorPrimary, mColorPrimaryDark, mColorAccent, mThemeType);
        } else if (view instanceof ViewGroup) {
            // call recursively
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                applyTheme(((ViewGroup) view).getChildAt(i));
            }
        }
    }

    public void applyColorPrimary(View view) {
        if (view instanceof ThemeCallback) {
            // call specific interface methods
            ((ThemeCallback) view).applyColorPrimary(mColorPrimary);
        } else if (view instanceof ViewGroup) {
            // call recursively
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                applyColorPrimary(((ViewGroup) view).getChildAt(i));
            }
        }
    }

    public void applyColorPrimaryDark(View view) {
        if (view instanceof ThemeCallback) {
            // call specific interface methods
            ((ThemeCallback) view).applyColorPrimaryDark(mColorPrimaryDark);
        } else if (view instanceof ViewGroup) {
            // call recursively
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                applyColorPrimaryDark(((ViewGroup) view).getChildAt(i));
            }
        }
    }

    public void applyColorAccent(View view) {
        if (view instanceof ThemeCallback) {
            // call specific interface methods
            ((ThemeCallback) view).applyColorAccent(mColorAccent);
        } else if (view instanceof ViewGroup) {
            // call recursively
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                applyColorAccent(((ViewGroup) view).getChildAt(i));
            }
        }
    }

    public void applyThemeType(View view) {
        if (view instanceof ThemeCallback) {
            // call specific interface methods
            ((ThemeCallback) view).applyThemeType(mThemeType);
        } else if (view instanceof ViewGroup) {
            // call recursively
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                applyThemeType(((ViewGroup) view).getChildAt(i));
            }
        }
    }

    public boolean setColorPrimary(int colorPrimary) {
        return setColorPrimary(colorPrimary, false);
    }

    public boolean setColorPrimary(int colorPrimary, boolean autoGenerateDarkColor) {
        return setColorPrimary(colorPrimary, autoGenerateDarkColor, true);
    }

    public boolean setColorPrimary(int colorPrimary, boolean autoGenerateDarkColor, boolean persistent) {
        if (mColorPrimary != colorPrimary) {
            if (persistent) {
                Preferences.setColorPrimary(mContext, colorPrimary);
            }
            mColorPrimary = colorPrimary;
            notifyColorPrimaryChanged();
            if (autoGenerateDarkColor) {
                setColorPrimaryDark(ColorUtils.darkenColor(mColorPrimary), persistent);
            }
            return true;
        }
        return false;
    }

    public boolean setColorPrimaryDark(int colorPrimaryDark) {
        return setColorPrimaryDark(colorPrimaryDark, true);
    }

    public boolean setColorPrimaryDark(int colorPrimaryDark, boolean persistent) {
        if (mColorPrimaryDark != colorPrimaryDark) {
            if (persistent) {
                Preferences.setColorPrimaryDark(mContext, colorPrimaryDark);
            }
            mColorPrimaryDark = colorPrimaryDark;
            notifyColorPrimaryDarkChanged();
            return true;
        }
        return false;
    }

    public boolean setColorAccent(int colorAccent) {
        return setColorAccent(colorAccent, true);
    }

    public boolean setColorAccent(int colorAccent, boolean persistent) {
        if (mColorAccent != colorAccent) {
            if (persistent) {
                Preferences.setColorAccent(mContext, colorAccent);
            }
            mColorAccent = colorAccent;
            notifyColorAccentChanged();
            return true;
        }
        return false;
    }

    public boolean setThemeType(ThemeType themeType) {
        return setThemeType(themeType, true);
    }

    public boolean setThemeType(ThemeType themeType, boolean persistent) {
        if (mThemeType != themeType) {
            if (persistent) {
                Preferences.setThemeType(mContext, themeType);
            }
            mThemeType = themeType;
            notifyThemeTypeChanged();
            return true;
        }
        return false;
    }

    public void notifyColorPrimaryChanged() {
        for (ThemeLifeCycle lifeCycle : mLifeCycles) {
            lifeCycle.onColorPrimaryChanged(mColorPrimary);
        }
    }

    public void notifyColorPrimaryDarkChanged() {
        for (ThemeLifeCycle lifeCycle : mLifeCycles) {
            lifeCycle.onColorPrimaryDarkChanged(mColorPrimaryDark);
        }
    }

    public void notifyColorAccentChanged() {
        for (ThemeLifeCycle lifeCycle : mLifeCycles) {
            lifeCycle.onColorAccentChanged(mColorAccent);
        }
    }

    public void notifyThemeTypeChanged() {
        for (ThemeLifeCycle lifeCycle : mLifeCycles) {
            lifeCycle.onThemeTypeChanged(mThemeType);
        }
    }

    public interface ThemeColorCallback {

        int getColorPrimary();

        int getColorPrimaryDark();

        int getColorAccent();

        ThemeType getThemeType();

        int getDisabledColor();

        int getDividerColor();

        int getHintColor();

        int getIconColor();

        int getTextPrimaryColor();

        int getTextSecondaryColor();

        int getWindowBackgroundColor();
    }

    public interface ThemeLifeCycle {

        void onColorPrimaryChanged(int colorPrimary);

        void onColorPrimaryDarkChanged(int colorPrimaryDark);

        void onColorAccentChanged(int colorAccent);

        void onThemeTypeChanged(ThemeType themeType);
    }
    
    public interface ThemeCallback {

        void applyTheme(int colorPrimary, int colorPrimaryDark, int colorAccent, ThemeType themeType);
        
        void applyColorPrimary(int colorPrimary);
        
        void applyColorPrimaryDark(int colorPrimaryDark);
        
        void applyColorAccent(int colorAccent);
        
        void applyThemeType(ThemeType themeType);
    }

    public static class Builder {

        private Context mContext;
        private Integer mColorPrimary;
        private Integer mColorPrimaryDark;
        private Integer mColorAccent;
        private ThemeType mThemeType;
        private int[] mTextPrimaryColors;
        private int[] mTextSecondaryColors;
        private int[] mWindowBackgroundColors;
        private int[] mDisabledHintColors;
        private int[] mDividersColors;

        public Builder(Context context) {
            mContext = context.getApplicationContext();
        }

        public Builder defaultColorPrimary(int colorPrimary) {
            mColorPrimary = colorPrimary;
            return this;
        }

        public Builder defaultColorPrimaryDark(int colorPrimaryDark) {
            mColorPrimaryDark = colorPrimaryDark;
            return this;
        }

        public Builder defaultColorAccent(int colorAccent) {
            mColorAccent = colorAccent;
            return this;
        }

        public Builder defaultThemeType(ThemeType themeType) {
            mThemeType = themeType;
            return this;
        }

        public Builder textPrimaryColors(int[] colors) {
            throwIfNot(colors, ThemeType.values().length);
            mTextPrimaryColors = colors;
            return this;
        }

        public Builder textSecondaryColors(int[] colors) {
            throwIfNot(colors, ThemeType.values().length);
            mTextSecondaryColors = colors;
            return this;
        }

        public Builder windowBackgroundColors(int[] colors) {
            throwIfNot(colors, ThemeType.values().length);
            mWindowBackgroundColors = colors;
            return this;
        }

        public Builder disabledHintColors(int[] colors) {
            throwIfNot(colors, ThemeType.values().length);
            mDisabledHintColors = colors;
            return this;
        }

        public Builder dividersColors(int[] colors) {
            throwIfNot(colors, ThemeType.values().length);
            mDividersColors = colors;
            return this;
        }

        private void throwIfNot(int[] colors, int required) {
            if (colors == null || colors.length != required) {
                throw new IllegalArgumentException("You must provide a valid array of " + String.valueOf(required) + " colors");
            }
        }
    }
}
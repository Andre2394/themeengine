/*
 * Copyright (c) 2016 AndreAle
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package it.andreale.theme.engine;

import android.content.Context;
import android.preference.PreferenceManager;
import android.graphics.Color;

import it.andreale.theme.R;
import it.andreale.theme.ThemeType;
import it.andreale.theme.util.ColorUtils;

/**
 * Created by AndreAle on 03/01/2016.
 */
public class Preferences {

    private final static String COLOR_PRIMARY = "ThemeEngine:ColorPrimary";
    private final static String COLOR_PRIMARY_DARK = "ThemeEngine:ColorPrimaryDark";
    private final static String COLOR_ACCENT = "ThemeEngine:ColorAccent";
    private final static String THEME_TYPE = "ThemeEngine:ThemeType";

    public static void setColorPrimary(Context context, int color) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(COLOR_PRIMARY, color).apply();
    }

    public static void setColorPrimaryDark(Context context, int color) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(COLOR_PRIMARY_DARK, color).apply();
    }

    public static void setColorAccent(Context context, int color) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(COLOR_ACCENT, color).apply();
    }

    public static void setThemeType(Context context, ThemeType themeType) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(THEME_TYPE, themeType.toString()).apply();
    }

    public static int getColorPrimary(Context context, Integer defaultColor) {
        int color = defaultColor != null ? defaultColor : getThemeColorPrimary(context);
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(COLOR_PRIMARY, color);
    }

    public static int getColorPrimaryDark(Context context, Integer defaultColor) {
        int color = defaultColor != null ? defaultColor : getThemeColorPrimaryDark(context);
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(COLOR_PRIMARY_DARK, color);
    }

    public static int getColorAccent(Context context, Integer defaultColor) {
        int color = defaultColor != null ? defaultColor : getThemeColorAccent(context);
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(COLOR_ACCENT, color);
    }

    public static ThemeType getThemeType(Context context, ThemeType defaultType) {
        String type = defaultType != null ? defaultType.toString() : ThemeType.THEME_LIGHT.toString();
        return ThemeType.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getString(THEME_TYPE, type));
    }

    public static int getThemeColorPrimary(Context context) {
        return ColorUtils.resolveColor(context, R.attr.colorPrimary, Color.WHITE);
    }

    public static int getThemeColorPrimaryDark(Context context) {
        return ColorUtils.resolveColor(context, R.attr.colorPrimaryDark, Color.WHITE);
    }

    public static int getThemeColorAccent(Context context) {
        return ColorUtils.resolveColor(context, R.attr.colorAccent, Color.WHITE);
    }
}
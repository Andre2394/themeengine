/*
 * Copyright (c) 2016 AndreAle
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package it.andreale.theme.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.RadioButton;
import android.widget.EditText;
import android.graphics.PorterDuff;
import java.lang.reflect.Field;

import it.andreale.theme.R;

/**
 * Created by AndreAle on 03/01/2016.
 */
public class TintHelper {

    public static void setTint(@NonNull AppCompatSpinner spinner, @ColorInt int colorFocused) {
        setTint(spinner, colorFocused, ColorUtils.resolveColor(spinner.getContext(), R.attr.colorControlNormal));
    }

    public static void setTint(@NonNull AppCompatSpinner spinner, @ColorInt int colorPressed, @ColorInt int colorUnpressed) {
        // generate state list
        int[][] states = new int[][] {
                new int[]{-android.R.attr.state_pressed},
                new int[]{android.R.attr.state_pressed}
        };
        int[] colors = new int[] {
                colorUnpressed,
                colorPressed
        };
        ColorStateList stateList = new ColorStateList(states, colors);
        spinner.setSupportBackgroundTintList(stateList);
    }

    @SuppressLint("PrivateResource")
    public static void setTint(@NonNull RadioButton radioButton, @ColorInt int colorChecked, @ColorInt int colorUnchecked) {
        // generate state list
        int[][] states = new int[][] {
                new int[]{-android.R.attr.state_checked},
                new int[]{android.R.attr.state_checked}
        };
        int[] colors = new int[] {
                colorUnchecked,
                colorChecked
        };
        ColorStateList stateList = new ColorStateList(states, colors);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            radioButton.setButtonTintList(stateList);
        } else {
            Drawable drawable = getDrawable(radioButton.getContext(), R.drawable.abc_btn_radio_material);
            DrawableCompat.setTintList(drawable, stateList);
            radioButton.setButtonDrawable(drawable);
        }
    }

    @SuppressLint("PrivateResource")
    public static void setTint(@NonNull RadioButton radioButton, @ColorInt int colorChecked) {
        setTint(radioButton, colorChecked, ColorUtils.resolveColor(radioButton.getContext(), R.attr.colorControlNormal));
    }

    @SuppressLint("PrivateResource")
    public static void setTint(@NonNull CheckBox checkBox, @ColorInt int colorChecked, @ColorInt int colorUnchecked) {
        // generate state list
        int[][] states = new int[][] {
                new int[]{-android.R.attr.state_checked},
                new int[]{android.R.attr.state_checked}
        };
        int[] colors = new int[] {
                colorUnchecked,
                colorChecked
        };
        ColorStateList stateList = new ColorStateList(states, colors);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            checkBox.setButtonTintList(stateList);
        } else {
            Drawable drawable = getDrawable(checkBox.getContext(), R.drawable.abc_btn_check_material);
            DrawableCompat.setTintList(drawable, stateList);
            checkBox.setButtonDrawable(drawable);
        }
    }

    public static void setTint(@NonNull AppCompatEditText editText, @ColorInt int colorFocused) {
        setTint(editText, colorFocused, ColorUtils.resolveColor(editText.getContext(), R.attr.colorControlNormal));
    }

    public static void setTint(@NonNull AppCompatEditText editText, @ColorInt int colorFocused, @ColorInt int colorUnfocused) {
        int[][] states = new int[][] {
                new int[]{-android.R.attr.state_enabled},
                new int[]{-android.R.attr.state_pressed, -android.R.attr.state_focused},
                new int[]{}
        };
        int[] colors = new int[] {
                colorUnfocused,
                colorUnfocused,
                colorFocused
        };
        ColorStateList stateList = new ColorStateList(states, colors);
        editText.setSupportBackgroundTintList(stateList);
        setCursorDrawableColor(editText, colorFocused);
    }

    @SuppressLint("PrivateResource")
    public static void setTint(@NonNull CheckBox checkBox, @ColorInt int colorChecked) {
        setTint(checkBox, colorChecked, ColorUtils.resolveColor(checkBox.getContext(), R.attr.colorControlNormal));
    }

    private static Drawable getDrawable(Context context, int resource) {
        return DrawableCompat.wrap(ContextCompat.getDrawable(context, resource));
    }

    public static void setCursorDrawableColor(EditText editText, int color) {
        try {
            Field fCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            fCursorDrawableRes.setAccessible(true);
            int mCursorDrawableRes = fCursorDrawableRes.getInt(editText);
            Field fEditor = TextView.class.getDeclaredField("mEditor");
            fEditor.setAccessible(true);
            Object editor = fEditor.get(editText);
            Class<?> clazz = editor.getClass();
            Field fCursorDrawable = clazz.getDeclaredField("mCursorDrawable");
            fCursorDrawable.setAccessible(true);
            Drawable[] drawables = new Drawable[2];
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                drawables[0] = editText.getContext().getResources().getDrawable(mCursorDrawableRes, editText.getContext().getTheme());
                drawables[1] = editText.getContext().getResources().getDrawable(mCursorDrawableRes, editText.getContext().getTheme());
            } else {
                drawables[0] = editText.getContext().getResources().getDrawable(mCursorDrawableRes);
                drawables[1] = editText.getContext().getResources().getDrawable(mCursorDrawableRes);
            }
            drawables[0].setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
            drawables[1].setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
            fCursorDrawable.set(editor, drawables);
        } catch (Exception e) {
            Log.v("Exception", e.getMessage());
        }
    }
}
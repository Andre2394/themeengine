/*
 * Copyright (c) 2016 AndreAle
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package it.andreale.theme.util;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.AttrRes;
import android.support.annotation.ColorInt;
import android.support.annotation.FloatRange;
import android.graphics.Color;

/**
 * Created by AndreAle on 03/01/2016.
 */
public class ColorUtils {

    public static int[] getDefaultTextPrimaryColors() {
        return new int[] {
                Color.parseColor("#212121"),
                Color.parseColor("#ffffff"),
                Color.parseColor("#ffffff")
        };
    }

    public static int[] getDefaultTextSecondaryColors() {
        return new int[] {
                Color.parseColor("#737373"),
                Color.parseColor("#bcbcbc"),
                Color.parseColor("#bcbcbc")
        };
    }

    public static int[] getDefaultWindowBackgroundColors() {
        return new int[] {
                Color.parseColor("#ffffff"),
                Color.parseColor("#212121"),
                Color.parseColor("#000000")
        };
    }

    public static int[] getDefaultDisabledHintColors() {
        return new int[] {
                Color.parseColor("#9b9b9b"),
                Color.parseColor("#646464"),
                Color.parseColor("#646464")
        };
    }

    public static int[] getDefaultDividersColors() {
        return new int[] {
                Color.parseColor("#dbdbdb"),
                Color.parseColor("#3c3c3c"),
                Color.parseColor("#3c3c3c")
        };
    }

    @ColorInt
    private static int shiftColor(@ColorInt int color, @FloatRange(from = 0.0f, to = 2.0f) float by) {
        if (by == 1f) return color;
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] *= by; // value component
        return Color.HSVToColor(hsv);
    }

    @ColorInt
    public static int darkenColor(@ColorInt int color) {
        return shiftColor(color, 0.9f);
    }

    @ColorInt
    public static int lightenColor(@ColorInt int color) {
        return shiftColor(color, 1.1f);
    }

    public static int resolveColor(Context context, @AttrRes int attr) {
        return resolveColor(context, attr, 0);
    }

    public static int resolveColor(Context context, @AttrRes int attr, int fallback) {
        TypedArray a = context.getTheme().obtainStyledAttributes(new int[]{attr});
        try {
            return a.getColor(0, fallback);
        } finally {
            a.recycle();
        }
    }

    public static String getHexColor(int color) {
        return String.format("#%06X", (0xFFFFFF & color));
    }

    public static boolean isColorLight(int color) {
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return red * 0.299 + green * 0.587 + blue * 0.114 > 186;
    }
}

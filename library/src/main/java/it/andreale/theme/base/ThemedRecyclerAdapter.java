/*
 * Copyright (c) 2016 AndreAle
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package it.andreale.theme.base;

import android.content.Context;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import it.andreale.theme.ThemeType;
import it.andreale.theme.engine.ThemeEngine;

/**
 * Created by AndreAle on 04/01/2016.
 */
public abstract class ThemedRecyclerAdapter<T extends ThemedRecyclerAdapter.ViewHolder> extends RecyclerView.Adapter<T> implements ThemeEngine.ThemeColorCallback, AdapterThemeNotifier {

    private ThemeEngine mThemeEngine;

    public ThemedRecyclerAdapter() {
        mThemeEngine = ThemeEngine.getInstance();
    }

    @CallSuper
    @Override
    public void onBindViewHolder(T holder, int position) {
        onApplyTheme(holder);
    }

    protected void onApplyTheme(T holder) {
        holder.applyTheme();
    }

    @Override
    public int getColorPrimary() {
        return mThemeEngine.getColorPrimary();
    }

    @Override
    public int getColorPrimaryDark() {
        return mThemeEngine.getColorPrimaryDark();
    }

    @Override
    public int getColorAccent() {
        return mThemeEngine.getColorAccent();
    }

    @Override
    public ThemeType getThemeType() {
        return mThemeEngine.getThemeType();
    }

    protected ThemeEngine getThemeEngine() {
        return mThemeEngine;
    }

    @Override
    public int getDisabledColor() {
        return mThemeEngine.getDisabledColor();
    }

    @Override
    public int getDividerColor() {
        return mThemeEngine.getDividerColor();
    }

    @Override
    public int getHintColor() {
        return mThemeEngine.getHintColor();
    }

    @Override
    public int getIconColor() {
        return mThemeEngine.getIconColor();
    }

    @Override
    public int getTextPrimaryColor() {
        return mThemeEngine.getTextPrimaryColor();
    }

    @Override
    public int getTextSecondaryColor() {
        return mThemeEngine.getTextSecondaryColor();
    }

    @Override
    public int getWindowBackgroundColor() {
        return mThemeEngine.getWindowBackgroundColor();
    }

    @Override
    public void notifyThemeChanged() {
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        public ViewHolder(@NonNull ViewGroup parent, @LayoutRes int resource) {
            super(LayoutInflater.from(parent.getContext()).inflate(resource, parent, false));
        }

        protected void applyTheme() {
            mThemeEngine.applyTheme(itemView);
        }

        public View findViewById(@IdRes int id) {
            return itemView.findViewById(id);
        }

        public Context getContext() {
            return itemView.getContext();
        }
    }
}
/*
 * Copyright (c) 2016 AndreAle
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package it.andreale.theme.base;

import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.content.Context;

import it.andreale.theme.ThemeType;
import it.andreale.theme.engine.ThemeEngine;

/**
 * Created by AndreAle on 04/01/2016.
 */
public abstract class ThemedListAdapter<T extends ThemedListAdapter.ViewHolder> extends BaseAdapter implements ThemeEngine.ThemeColorCallback, AdapterThemeNotifier {

    private ThemeEngine mThemeEngine;

    public ThemedListAdapter() {
        mThemeEngine = ThemeEngine.getInstance();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        T viewHolder;
        if (view == null) {
            viewHolder = onCreateViewHolder(parent, position);
            view = viewHolder.getView();
            view.setTag(viewHolder);
        } else {
            //noinspection unchecked
            viewHolder = (T) view.getTag();
        }
        viewHolder.applyTheme();
        onBindViewHolder(viewHolder, position);
        return view;
    }

    @NonNull
    protected abstract T onCreateViewHolder(ViewGroup parent, int position);

    protected abstract void onBindViewHolder(T holder, int position);

    @Override
    public int getColorPrimary() {
        return mThemeEngine.getColorPrimary();
    }

    @Override
    public int getColorPrimaryDark() {
        return mThemeEngine.getColorPrimaryDark();
    }

    @Override
    public int getColorAccent() {
        return mThemeEngine.getColorAccent();
    }

    @Override
    public ThemeType getThemeType() {
        return mThemeEngine.getThemeType();
    }

    protected ThemeEngine getThemeEngine() {
        return mThemeEngine;
    }

    @Override
    public int getDisabledColor() {
        return mThemeEngine.getDisabledColor();
    }

    @Override
    public int getDividerColor() {
        return mThemeEngine.getDividerColor();
    }

    @Override
    public int getHintColor() {
        return mThemeEngine.getHintColor();
    }

    @Override
    public int getIconColor() {
        return mThemeEngine.getIconColor();
    }

    @Override
    public int getTextPrimaryColor() {
        return mThemeEngine.getTextPrimaryColor();
    }

    @Override
    public int getTextSecondaryColor() {
        return mThemeEngine.getTextSecondaryColor();
    }

    @Override
    public int getWindowBackgroundColor() {
        return mThemeEngine.getWindowBackgroundColor();
    }

    @Override
    public void notifyThemeChanged() {
        notifyDataSetChanged();
    }

    public abstract class ViewHolder {

        private View mItemView;

        public ViewHolder(ViewGroup parent, @LayoutRes int resource) {
            mItemView = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        }

        public void applyTheme() {
            mThemeEngine.applyTheme(mItemView);
        }

        public View getView() {
            return mItemView;
        }

        public View findViewById(@IdRes int id) {
            return mItemView.findViewById(id);
        }

        public Context getContext() {
            return mItemView.getContext();
        }
    }
}
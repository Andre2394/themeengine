/*
 * Copyright (c) 2016 AndreAle
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package it.andreale.theme.base;

import android.app.ActivityManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;

import it.andreale.theme.ThemeType;
import it.andreale.theme.engine.ThemeEngine;
import it.andreale.theme.view.base.ThemedToolbar;

/**
 * Created by AndreAle on 03/01/2016.
 */
public abstract class ThemedActivity extends AppCompatActivity implements ThemeEngine.ThemeLifeCycle, ThemeEngine.ThemeColorCallback {

    private Toolbar mToolbar;
    private ThemeEngine mThemeEngine;

    @CallSuper
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mThemeEngine = ThemeEngine.getInstance();
        mThemeEngine.register(this);
        onCreateView(savedInstanceState);
        onApplyTheme();
    }

    protected abstract void onCreateView(@Nullable Bundle savedInstanceState);

    protected void onApplyTheme() {
        onApplyStatusBarColor(mThemeEngine.getColorPrimaryDark());
        View rootView = findViewById(android.R.id.content);
        rootView.setBackgroundColor(mThemeEngine.getWindowBackgroundColor());
        mThemeEngine.applyTheme(rootView);
    }

    protected void onApplyStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(color);
        }
    }

    @Override
    public void setSupportActionBar(Toolbar toolbar) {
        super.setSupportActionBar(toolbar);
        mToolbar = toolbar;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mToolbar instanceof ThemedToolbar) {
            // reapply color primary in order to apply child color to menu items after menu inflation
            ((ThemedToolbar) mToolbar).applyColorPrimary(getColorPrimary());
        }
        return super.onPrepareOptionsMenu(menu);
    }

    protected Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public int getColorPrimary() {
        return mThemeEngine.getColorPrimary();
    }

    @Override
    public int getColorPrimaryDark() {
        return mThemeEngine.getColorPrimaryDark();
    }

    @Override
    public int getColorAccent() {
        return mThemeEngine.getColorAccent();
    }

    @Override
    public ThemeType getThemeType() {
        return mThemeEngine.getThemeType();
    }

    protected ThemeEngine getThemeEngine() {
        return mThemeEngine;
    }

    @Override
    public int getDisabledColor() {
        return mThemeEngine.getDisabledColor();
    }

    @Override
    public int getDividerColor() {
        return mThemeEngine.getDividerColor();
    }

    @Override
    public int getHintColor() {
        return mThemeEngine.getHintColor();
    }

    @Override
    public int getIconColor() {
        return mThemeEngine.getIconColor();
    }

    @Override
    public int getTextPrimaryColor() {
        return mThemeEngine.getTextPrimaryColor();
    }

    @Override
    public int getTextSecondaryColor() {
        return mThemeEngine.getTextSecondaryColor();
    }

    @Override
    public int getWindowBackgroundColor() {
        return mThemeEngine.getWindowBackgroundColor();
    }

    @Override
    protected void onDestroy() {
        mThemeEngine.unregister(this);
        super.onDestroy();
    }

    @Override
    public void onColorPrimaryChanged(int colorPrimary) {
        mThemeEngine.applyColorPrimary(findViewById(android.R.id.content));
    }

    protected void setTaskDescription(String title, @DrawableRes int icon, int colorPrimary) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), icon);
            try {
                setTaskDescription(new ActivityManager.TaskDescription(title, bitmap, colorPrimary));
            } catch (RuntimeException e) {
                // color primary not supported
            }
        }
    }

    @Override
    public void onColorPrimaryDarkChanged(int colorPrimaryDark) {
        onApplyStatusBarColor(colorPrimaryDark);
        mThemeEngine.applyColorPrimaryDark(findViewById(android.R.id.content));
    }

    @Override
    public void onColorAccentChanged(int colorAccent) {
        mThemeEngine.applyColorAccent(findViewById(android.R.id.content));
    }

    @Override
    public void onThemeTypeChanged(ThemeType themeType) {
        View rootView = findViewById(android.R.id.content);
        rootView.setBackgroundColor(mThemeEngine.getWindowBackgroundColor());
        mThemeEngine.applyTheme(rootView);
    }
}
/*
 * Copyright (c) 2016 AndreAle
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package it.andreale.theme.view.base;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ListView;
import android.widget.ListAdapter;

import it.andreale.theme.ThemeType;
import it.andreale.theme.base.ThemedListAdapter;
import it.andreale.theme.engine.ThemeEngine;
import it.andreale.theme.util.EdgeGlowUtil;

/**
 * Created by Andrea on 04/01/2016.
 */
public class ThemedListView extends ListView implements ThemeEngine.ThemeCallback {

    private boolean mFirstApply;

    public ThemedListView(Context context) {
        super(context);
    }

    public ThemedListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ThemedListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ThemedListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void applyTheme(int colorPrimary, int colorPrimaryDark, int colorAccent, ThemeType themeType) {
        mFirstApply = true;
        applyColorPrimary(colorPrimary);
        applyColorPrimaryDark(colorPrimaryDark);
        applyColorAccent(colorAccent);
        applyThemeType(themeType);
        mFirstApply = false;
        notifyAdapterThemeIsChanged();
    }

    @Override
    public void applyColorPrimary(int colorPrimary) {
        EdgeGlowUtil.setEdgeGlowColor(this, colorPrimary);
        if (!mFirstApply) {
            notifyAdapterThemeIsChanged();
        }
    }

    @Override
    public void applyColorPrimaryDark(int colorPrimaryDark) {
        if (!mFirstApply) {
            notifyAdapterThemeIsChanged();
        }
    }

    @Override
    public void applyColorAccent(int colorAccent) {
        if (!mFirstApply) {
            notifyAdapterThemeIsChanged();
        }
    }

    @Override
    public void applyThemeType(ThemeType themeType) {
        if (!mFirstApply) {
            notifyAdapterThemeIsChanged();
        }
    }

    protected void notifyAdapterThemeIsChanged() {
        ListAdapter adapter = getAdapter();
        if (adapter instanceof ThemedListAdapter) {
            ((ThemedListAdapter) adapter).notifyDataSetChanged();
        }
    }
}

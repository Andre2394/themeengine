/*
 * Copyright (c) 2016 AndreAle
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package it.andreale.theme.view.base;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.widget.ActionMenuView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import it.andreale.theme.ThemeType;
import it.andreale.theme.engine.ThemeEngine;
import it.andreale.theme.util.ColorUtils;

/**
 * Created by Andrea on 03/01/2016.
 */
public class ThemedToolbar extends android.support.v7.widget.Toolbar implements ThemeEngine.ThemeCallback {

    public ThemedToolbar(Context context) {
        super(context);
    }

    public ThemedToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ThemedToolbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void applyTheme(int colorPrimary, int colorPrimaryDark, int colorAccent, ThemeType themeType) {
        applyColorPrimary(colorPrimary);
        applyColorPrimaryDark(colorPrimaryDark);
        applyColorAccent(colorAccent);
        applyThemeType(themeType);
    }

    @Override
    public void applyColorPrimary(int colorPrimary) {
        setBackgroundColor(colorPrimary);
        int childColor = getChildColor(colorPrimary);
        for (int i = 0; i < getChildCount(); i++) {
            View view = getChildAt(i);
            if (view instanceof TextView) {
                ((TextView) view).setTextColor(childColor);
            } else if (view instanceof ActionMenuView) {
                for (int n = 0; n < ((ActionMenuView) view).getChildCount(); n++) {
                    View childView = ((ActionMenuView) view).getChildAt(n);
                    if (childView instanceof ActionMenuItemView) {
                        // apply color filter to all drawables
                        Drawable[] drawables = ((ActionMenuItemView) childView).getCompoundDrawables();
                        for (Drawable drawable : drawables) {
                            if (drawable != null) {
                                drawable.setColorFilter(childColor, PorterDuff.Mode.SRC_ATOP);
                            }
                        }
                    }
                }
                onTintOverflowMenuIcon((ActionMenuView) view, childColor);
            } else if (view instanceof ImageButton) {
                ((ImageButton) view).getDrawable().setColorFilter(childColor, PorterDuff.Mode.SRC_ATOP);
            }  else if (view instanceof ImageView) {
                ((ImageView) view).getDrawable().setColorFilter(childColor, PorterDuff.Mode.SRC_ATOP);
            } else {
                Log.v("TAG", view.getClass().getName());
            }
        }
    }

    @Override
    public void applyColorPrimaryDark(int colorPrimaryDark) {
        // not used
    }

    @Override
    public void applyColorAccent(int colorAccent) {
        // not used
    }

    @Override
    public void applyThemeType(ThemeType themeType) {
        // not used
    }

    private int getChildColor(int backgroundColor) {
        return ColorUtils.isColorLight(backgroundColor) ? Color.BLACK : Color.WHITE;
    }

    protected void onTintOverflowMenuIcon(ActionMenuView actionMenuView, int color) {
        Drawable overflowIcon = actionMenuView.getOverflowIcon();
        if (overflowIcon != null) {
            overflowIcon.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        }
    }
}
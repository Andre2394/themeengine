/*
 * Copyright (c) 2016 AndreAle
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package it.andreale.theme.view.base;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.annotation.ArrayRes;
import android.support.annotation.ColorInt;
import android.support.v7.widget.AppCompatSpinner;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.List;

import it.andreale.theme.ThemeType;
import it.andreale.theme.engine.ThemeEngine;
import it.andreale.theme.util.TintHelper;

/**
 * Created by Andrea on 04/01/2016.
 */
public class ThemedSpinner extends AppCompatSpinner implements ThemeEngine.ThemeCallback {

    private int mTextColor;

    public ThemedSpinner(Context context) {
        super(context);
        initialize();
    }

    public ThemedSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public ThemedSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    public ThemedSpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode) {
        super(context, attrs, defStyleAttr, mode);
        initialize();
    }

    public ThemedSpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode, Resources.Theme popupTheme) {
        super(context, attrs, defStyleAttr, mode, popupTheme);
        initialize();
    }

    private void initialize() {
        mTextColor = Color.BLACK;
    }

    public void setTextColor(int color) {
        mTextColor = color;
        SpinnerAdapter adapter = getAdapter();
        if (adapter instanceof ThemedSpinnerAdapter) {
            ((ThemedSpinnerAdapter) adapter).notifyDataSetChanged();
        }
    }

    @Override
    public void applyTheme(int colorPrimary, int colorPrimaryDark, int colorAccent, ThemeType themeType) {
        applyColorPrimary(colorPrimary);
        applyColorPrimaryDark(colorPrimaryDark);
        applyColorAccent(colorAccent);
        applyThemeType(themeType);
    }

    @Override
    public void applyColorPrimary(int colorPrimary) {
        // not used
    }

    @Override
    public void applyColorPrimaryDark(int colorPrimaryDark) {
        // not used
    }

    @Override
    public void applyColorAccent(int colorAccent) {
        TintHelper.setTint(this, colorAccent);
    }

    @Override
    public void applyThemeType(ThemeType themeType) {
        // not used
    }

    public void setItems(CharSequence[] items) {
        createAdapter(getContext(), items);
    }

    public void setItems(CharSequence[] items, @ColorInt int textColor) {
        mTextColor = textColor;
        createAdapter(getContext(), items);
    }

    public void setItems(@ArrayRes int resource) {
        createAdapter(getContext(), resource);
    }

    public void setItems(@ArrayRes int resource, @ColorInt int textColor) {
        mTextColor = textColor;
        createAdapter(getContext(), resource);
    }

    protected void createAdapter(Context context, CharSequence[] items) {
        ThemedSpinnerAdapter adapter = new ThemedSpinnerAdapter(context, android.R.layout.simple_spinner_item, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        setAdapter(adapter);
    }

    protected void createAdapter(Context context, @ArrayRes int resource) {
        createAdapter(context, context.getResources().getTextArray(resource));
    }

    public class ThemedSpinnerAdapter extends ArrayAdapter<CharSequence> {

        public ThemedSpinnerAdapter(Context context, int resource) {
            super(context, resource);
        }

        public ThemedSpinnerAdapter(Context context, int resource, int textViewResourceId) {
            super(context, resource, textViewResourceId);
        }

        public ThemedSpinnerAdapter(Context context, int resource, CharSequence[] objects) {
            super(context, resource, objects);
        }

        public ThemedSpinnerAdapter(Context context, int resource, int textViewResourceId, CharSequence[] objects) {
            super(context, resource, textViewResourceId, objects);
        }

        public ThemedSpinnerAdapter(Context context, int resource, List<CharSequence> objects) {
            super(context, resource, objects);
        }

        public ThemedSpinnerAdapter(Context context, int resource, int textViewResourceId, List<CharSequence> objects) {
            super(context, resource, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView view = (TextView) super.getView(position, convertView, parent);
            view.setTextColor(mTextColor);
            return view;
        }
    }
}
/*
 * Copyright (c) 2016 AndreAle
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package it.andreale.theme.view.base;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ScrollView;

import it.andreale.theme.ThemeType;
import it.andreale.theme.engine.ThemeEngine;
import it.andreale.theme.util.EdgeGlowUtil;

/**
 * Created by Andrea on 04/01/2016.
 */
public class ThemedScrollView extends ScrollView implements ThemeEngine.ThemeCallback {

    private ThemeEngine mThemeEngine;

    public ThemedScrollView(Context context) {
        super(context);
        initialize();
    }

    public ThemedScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public ThemedScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ThemedScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize();
    }

    private void initialize() {
        mThemeEngine = ThemeEngine.getInstance();
    }

    @Override
    public void applyTheme(int colorPrimary, int colorPrimaryDark, int colorAccent, ThemeType themeType) {
        for (int i = 0; i < getChildCount(); i++) {
            mThemeEngine.applyTheme(getChildAt(i));
        }
    }

    @Override
    public void applyColorPrimary(int colorPrimary) {
        EdgeGlowUtil.setEdgeGlowColor(this, colorPrimary);
        // propagate signal to all child
        for (int i = 0; i < getChildCount(); i++) {
            mThemeEngine.applyColorPrimary(getChildAt(i));
        }
    }

    @Override
    public void applyColorPrimaryDark(int colorPrimaryDark) {
        // propagate signal to all child
        for (int i = 0; i < getChildCount(); i++) {
            mThemeEngine.applyColorPrimaryDark(getChildAt(i));
        }
    }

    @Override
    public void applyColorAccent(int colorAccent) {
        // propagate signal to all child
        for (int i = 0; i < getChildCount(); i++) {
            mThemeEngine.applyColorAccent(getChildAt(i));
        }
    }

    @Override
    public void applyThemeType(ThemeType themeType) {
        // propagate signal to all child
        for (int i = 0; i < getChildCount(); i++) {
            mThemeEngine.applyThemeType(getChildAt(i));
        }
    }
}

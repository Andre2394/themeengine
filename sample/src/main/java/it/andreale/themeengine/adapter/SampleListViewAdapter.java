/*
 * Copyright (c) 2016 AndreAle
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package it.andreale.themeengine.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import it.andreale.theme.base.ThemedListAdapter;
import it.andreale.themeengine.R;

/**
 * Created by AndreAle on 04/01/2016.
 */
public class SampleListViewAdapter extends ThemedListAdapter<SampleListViewAdapter.ViewHolder> {

    private ArrayList<String> mItems;

    public SampleListViewAdapter() {
        mItems = new ArrayList<>();
    }

    public void add(String object) {
        mItems.add(object);
    }

    @NonNull
    @Override
    protected SampleListViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        return new ViewHolder(parent, R.layout.simple_list_item_1);
    }

    @Override
    protected void onBindViewHolder(SampleListViewAdapter.ViewHolder holder, int position) {
        holder.setText(getItem(position));
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public String getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder extends ThemedListAdapter.ViewHolder {

        private TextView mTextView;

        public ViewHolder(ViewGroup parent, @LayoutRes int resource) {
            super(parent, resource);
            mTextView = (TextView) getView();
        }

        public void setText(String text) {
            mTextView.setText(text);
        }
    }
}

/*
 * Copyright (c) 2016 AndreAle
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package it.andreale.themeengine.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import it.andreale.theme.base.ThemedRecyclerAdapter;
import it.andreale.themeengine.R;

/**
 * Created by AndreAle on 04/01/2016.
 */
public class ArrayAdapter extends ThemedRecyclerAdapter<ArrayAdapter.ViewHolder> {

    private ArrayList<String> mItems;

    public ArrayAdapter() {
        mItems = new ArrayList<>();
    }

    public void add(String object) {
        mItems.add(object);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(parent, R.layout.simple_list_item_1);
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        holder.setText(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class ViewHolder extends ThemedRecyclerAdapter.ViewHolder {

        public ViewHolder(@NonNull ViewGroup parent, @LayoutRes int resource) {
            super(parent, resource);
        }

        public void setText(String text) {
            ((TextView) itemView).setText(text);
        }
    }
}
/*
 * Copyright (c) 2016 AndreAle
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package it.andreale.themeengine.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

import it.andreale.theme.base.ThemedActivity;
import it.andreale.theme.base.ThemedPreferenceFragment;
import it.andreale.themeengine.R;

public class ActivitySetting extends ThemedActivity {

    private final static String FRAGMENT_TAG = "preference_tag";

    @Override
    protected void onCreateView(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_setting);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        SettingFragment settingFragment = (SettingFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);
        if (settingFragment == null) {
            settingFragment = new SettingFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, settingFragment, FRAGMENT_TAG)
                    .commit();
        }
    }

    public static class SettingFragment extends ThemedPreferenceFragment {

        // TODO
    }
}
/*
 * Copyright (c) 2016 AndreAle
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package it.andreale.themeengine.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import it.andreale.theme.ThemeType;
import it.andreale.theme.base.ThemedActivity;
import it.andreale.theme.util.ColorUtils;
import it.andreale.theme.view.base.ThemedListView;
import it.andreale.theme.view.primary.PrimaryEditText;
import it.andreale.theme.view.primary.PrimarySpinner;
import it.andreale.themeengine.R;
import it.andreale.themeengine.adapter.SampleListViewAdapter;

/**
 * Created by AndreAle on 03/01/2016.
 */
public class MainActivity extends ThemedActivity implements RadioGroup.OnCheckedChangeListener {

    private PrimaryEditText colorPrimary;
    private PrimaryEditText colorAccent;

    @Override
    protected void onCreateView(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        colorPrimary = (PrimaryEditText) findViewById(R.id.color_primary);
        findViewById(R.id.color_primary_apply).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    int color = Color.parseColor(colorPrimary.getText().toString());
                    getThemeEngine().setColorPrimary(color, true);
                } catch (IllegalArgumentException ignore) {
                    // color not valid, restore old color
                    colorPrimary.setText(ColorUtils.getHexColor(getColorPrimary()));
                }
            }

        });
        colorPrimary.setText(ColorUtils.getHexColor(getColorPrimary()));
        colorAccent = (PrimaryEditText) findViewById(R.id.color_accent);
        findViewById(R.id.color_accent_apply).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    int color = Color.parseColor(colorAccent.getText().toString());
                    getThemeEngine().setColorAccent(color);
                } catch (IllegalArgumentException ignore) {
                    // color not valid, restore old color
                    colorAccent.setText(ColorUtils.getHexColor(getColorAccent()));
                }
            }

        });
        colorAccent.setText(ColorUtils.getHexColor(getColorAccent()));
        PrimarySpinner spinner = (PrimarySpinner) findViewById(R.id.spinner);
        spinner.setItems(R.array.items);
        RadioGroup group = (RadioGroup) findViewById(R.id.theme_type);
        switch (getThemeType()) {
            case THEME_LIGHT:
                ((RadioButton) findViewById(R.id.theme_light)).setChecked(true);
                break;
            case THEME_DARK:
                ((RadioButton) findViewById(R.id.theme_dark)).setChecked(true);
                break;
            case THEME_DEEP_DARK:
                ((RadioButton) findViewById(R.id.theme_deep_dark)).setChecked(true);
                break;
        }
        group.setOnCheckedChangeListener(this);
        /*
        ThemedRecyclerView mRecyclerView = (ThemedRecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        ArrayAdapter adapter = new ArrayAdapter();
        for (int i = 0; i < 50; i++) {
            adapter.add("Item " + String.valueOf(i));
        }
        mRecyclerView.setAdapter(adapter); */
        ThemedListView listView = (ThemedListView) findViewById(R.id.list_view);
        SampleListViewAdapter adapter = new SampleListViewAdapter();
        for (int i = 0; i < 50; i++) {
            adapter.add("Item " + String.valueOf(i));
        }
        listView.setAdapter(adapter);

        // apply task description
        setTaskDescription(getTitle().toString(), R.mipmap.ic_launcher, getColorPrimary());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.setting:
                Intent i = new Intent(this, ActivitySetting.class);
                startActivity(i);
        }
        return true;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.theme_light:
                getThemeEngine().setThemeType(ThemeType.THEME_LIGHT);
                break;
            case R.id.theme_dark:
                getThemeEngine().setThemeType(ThemeType.THEME_DARK);
                break;
            case R.id.theme_deep_dark:
                getThemeEngine().setThemeType(ThemeType.THEME_DEEP_DARK);
                break;
        }
    }

    @Override
    public void onColorPrimaryChanged(int colorPrimary) {
        super.onColorPrimaryChanged(colorPrimary);
        setTaskDescription(getTitle().toString(), R.mipmap.ic_launcher, colorPrimary);
    }
}
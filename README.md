# ThemeEngine for Android Material Design apps #

Questa libreria ti aiuterà ad implementare un motore grafico nella tua applicazione. Potrai gestire con facilità i colori del tema in modo completamente dinamico. In poco meno di 5 minuti sarà tutto pronto all'uso.

### Setup ###
Inizializziamo il ThemeEngine nella classe Application
```
#!java
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // if something is not specified, default value will be used by ThemeEngine.class
        ThemeEngine.initialize(
                new ThemeEngine.Builder(this)
                        .defaultColorPrimary(int color)
                        .defaultColorPrimaryDark(int color)
                        .defaultColorAccent(int color)
                        .defaultThemeType(ThemeType themeType)
                        .textPrimaryColors(int[] colors)
                        .textSecondaryColors(int[] colors)
                        .disabledHintColors(int[] colors)
                        .dividersColors(int[] colors)
                        .windowBackgroundColors(int[] colors)
        );
    }
}
```
NB: ricorda di registrare la classe nel Manifest.xml

```
#!xml
<application
    android:allowBackup="true"
    android:icon="@mipmap/ic_launcher"
    android:label="@string/app_name"
    android:name=".App">

    ...

</application>
```
### Applicazione del tema ###

* Activity
```
#!java
public class MyActivity extends ThemedActivity {

    @Override
    protected void onCreateView(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.my_activity);
        // use this just like onCreate(Bundle);
    }
}
```

* Fragment
```
#!java
public class MyFragment extends ThemedFragment {

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // inflate or create view here just standard fragment.
        return new View();
    }
}
```

### Come funziona? ###

* Utilizzo standard
Il motore grafico cercherà automaticamente ogni view al quale potrà applicare il tema. Dovrai quindi usare le custom view presenti nella libreria al posto di quelle standard presenti nel android SDK.

* Utilizzo avanzato
Nel caso in cui hai bisogno di applicare il tema ad una custom view non dovrai fare altro che implementare l'interfaccia ThemeEngine.ThemeCallback. Cosi facendo i metodi della tua custom view verranno automaticamente chiamati durante l'applicazione del tema.
```
#!java
public class MyCustomView extends View implements ThemeEngine.ThemeCallback {

    .....

    @Override
    public void applyTheme(int colorPrimary, int colorPrimaryDark, int colorAccent, ThemeType themeType) {
        // apply theme the first time
    }

    @Override
    public void applyColorPrimary(int colorPrimary) {
        // apply color primary to custom view
    }

    @Override
    public void applyColorPrimaryDark(int colorPrimaryDark) {
        // apply color primary dark to custom view
    }

    @Override
    public void applyColorAccent(int colorAccent) {
        // apply color accent to custom view
    }

    @Override
    public void applyThemeType(ThemeType themeType) {
        // apply theme type to custom view
    }
}
```

### Quali view sono supportate di default dalla libreria? ###

***Toolbar***

* ThemedToolbar (applica colorPrimary sullo sfondo e Color.WHITE se colorPrimary è scuro o Color.BLACK se colorPrimary è chiaro a tutti i figli della view). Se viene utilizzata come ActionBar in una activity che estende ThemedActivity il menu verrà automaticamente colorato dopo la sua applicazione.

***TextView***

- PrimaryTextView (applica textPrimaryColor al testo)
- SecondaryTextView (applica textSecondaryColor al testo)

***CheckBox***

* ThemedCheckBox (applica colorAccent al widget)
* PrimaryCheckBox (applica colorAccent al widget e textPrimaryColor al testo)
* SecondaryCheckBox (applica colorAccent al widget e textSecondaryColor al testo)

***RadioButton***

* ThemedRadioButton (applica colorAccent al widget)
* PrimaryRadioButton (applica colorAccent al widget e textPrimaryColor al testo)
* SecondaryRadioButton (applica colorAccent al widget e textSecondaryColor al testo)

***EditText***

* ThemedEditText (applica colorAccent al widget)
* PrimaryEditText (applica colorAccent al widget e textPrimaryColor al testo)
* SecondaryEditText (applica colorAccent al widget e textSecondaryColor al testo)

** TODO: il cursore e il testo selezionato non vengono colorati, bisogna trovare un modo tramite reflection di accedere ai campi protetti della classe super e cambiarne i valori.

***Spinner***

* ThemedSpinner (applica colorAccent al widget)
* PrimarySpinner (applica colorAccent al widget e textPrimaryColor al testo)
* SecondarySpinner (applica colorAccent al widget e textSecondaryColor al testo)

***ScrollView***

* ThemedScrollView (applica colorPrimary alla view).
Provvederà automaticamente ad applicare il tema anche alle view figlie.

***ListView***

* ThemedListView (applica colorPrimary alla view).
Se le viene attaccato un adapter che estende ThemedListAdapter provvederà automaticamente ad applicare il tema anche alle view figlie.

***RecyclerView***

* ThemedRecyclerView (applica colorPrimary alla view).
Se le viene attaccato un adapter che estende ThemedAdapter provvederà automaticamente ad applicare il tema anche alle view figlie.

### Posso cambiare il tema dinamicamente? ###
Certo, estendendo la tua activity o il tuo fragment con quelli presenti nella libreria, i rispettivi metodi verranno chiamati nella fase di prima applicazione del tema e ogni qualvolta un parametro del tema cambi.

### Contribuisci ###
Questa libreria è stata resa open source per permettere a chiunque di contribuire. Se noti bug o vuoi aiutare non esitare. Mancano all'appello ancora molte view standard del SDK.